package com.lyon.springboot.plugin;

import java.util.List;

/**
 * //分页封装函数
 * 
 * @param <T>
 */
public class PageView {
	/**
	 * 分页数据
	 */
	private List<?> data;

	/**
	 * 总页数 这个数是计算出来的
	 * 
	 */
//	private long pageCount;

	/**
	 * 每页显示几条记录
	 */
	private int length = 10;

	/**
	 * 默认 当前页 为第一页 这个数是计算出来的
	 */
	private int draw = 1;

	/**
	 * 总记录数
	 */
	private long recordsTotal;
	/**
	 * 搜索记录数
	 */
	private long recordsFiltered;

	/**
	 * 从第几条记录开始
	 */
	private int start;

	/**
	 * 规定显示5个页码
	 */
//	private int pagecode = 10;

	public PageView() {
	}

	/**
	 * 要获得记录的开始索引　即　开始页码
	 * 
	 * @return
	 */
//	public int getFirstResult() {
//		return (this.draw - 1) * this.length;
//	}

//	public int getPagecode() {
//		return pagecode;
//	}
//
//	public void setPagecode(int pagecode) {
//		this.pagecode = pagecode;
//	}

	/**
	 * 使用构造函数，，强制必需输入 每页显示数量　和　当前页
	 * 
	 * @param pageSize
	 *            　　每页显示数量
	 * @param pageNow
	 *            　当前页
	 */
	public PageView(int length, int draw) {
		this.length = length;
		this.draw = draw;
	}

	/**
	 * 使用构造函数，，强制必需输入 当前页
	 * 
	 * @param pageNow
	 *            　当前页
	 */
	public PageView(int draw) {
		this.draw = draw;
//		start = (this.draw - 1) * this.length;
	}

	/**
	 * 查询结果方法 把　记录数　结果集合　放入到　PageView对象
	 * 
	 * @param rowCount
	 *            总记录数
	 * @param records
	 *            结果集合
	 */

	public void setQueryResult(long recordsTotal, List<?> data) {
		setRecordsTotal(recordsTotal);
		setData(data);
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
		setRecordsFiltered(recordsTotal);
//		setPageCount(this.recordsTotal % this.length == 0 ? this.recordsTotal / this.length : this.recordsTotal / this.length + 1);
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

//	public long getPageCount() {
//		return pageCount;
//	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

//	public void setPageCount(long pageCount) {
//		this.pageCount = pageCount;
//	}

	public long getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(long recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	@Override
	public String toString() {
		return "PageView [draw=" + draw + ", recordsTotal=" + recordsTotal + ", start=" + start + "]";
	}

}
