package com.lyon.springboot.mapper;

import com.lyon.springboot.model.User;

import java.util.List;

/**
 * 
 * @author LiuSongqing
 *
 */
public interface UserMapper {


	public Long save(User user);

	public User getById(Long id);

	public List<User> findList(User user);

	public Long deleteById(Long id);

	public Long delete(User user);

	public Long update(User user);

}
