package com.lyon.springboot.controller;

import com.alibaba.fastjson.JSONObject;
import com.lyon.springboot.model.User;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/demo")
public class DemoController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 可以直接使用@ResponseBody响应JSON
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getcount", method = RequestMethod.POST)
	@ApiOperation(value = "测试-getCount", notes = "getCount更多说明")
	public ModelMap getCount(HttpServletRequest request, HttpServletResponse response) {
		logger.info(">>>>>>>> begin getCount >>>>>>>>");
		ModelMap map = new ModelMap();
		map.addAttribute("count", 158);

		// 后台获取的国际化信息
		map.addAttribute("xstest", "测试");
		return map;
	}

	/**
	 * 可以直接使用@ResponseBody响应JSON
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	// @ApiIgnore//使用该注解忽略这个API
	@ResponseBody
	@RequestMapping(value = "/getJsonData", method = RequestMethod.POST)
	public ModelMap getJsonData(HttpServletRequest request, HttpServletResponse response) {
		ModelMap map = new ModelMap();
		map.addAttribute("hello", "你好");
		map.addAttribute("veryGood", "很好");

		return map;
	}

	/**
	 * 可以直接使用@ResponseBody响应JSON
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testForRequest", method = RequestMethod.POST)
	public List<String> testForRequest(HttpServletRequest request, HttpServletResponse response) {
		List<String> list = new ArrayList<String>();
		list.add("hello");
		list.add("你好");
		return list;
	}

	/**
	 * JSON请求一个对象<br/>
	 * （Ajax Post Data：{"name":"名称","age":"年龄"}）
	 * 
	 * @param version
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testForObject", method = RequestMethod.POST)
	public ModelMap testForObject(@RequestBody User user) {
		logger.info("userName：" + user.getName());
		logger.info("userAge：" + user.getAge());
		ModelMap map = new ModelMap();
		map.addAttribute("result", "ok");
		return map;
	}

	/**
	 * 直接读取URL参数值<br/>
	 * /demo/jsonTest6.do?name=Hello&content=World
	 * 
	 * @param demoName
	 * @param content
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testForRequestParam", method = RequestMethod.POST)
	public ModelMap testForRequestParam(@RequestParam("name") String demoName, @RequestParam String content) {
		logger.info("demoName：" + demoName);
		ModelMap map = new ModelMap();
		map.addAttribute("name", demoName + "AAA");
		map.addAttribute("content", content + "BBB");
		map.addAttribute("date", new java.util.Date());
		return map;
	}

	/**
	 * JSON请求一个对象，将RequestBody自动转换为JSONObject对象<br/>
	 * （Ajax Post Data：{"name":"名称","content":"内容"}）
	 * 
	 * 使用JSONObject请添加依赖 <dependency> <groupId>net.sf.json-lib</groupId>
	 * <artifactId>json-lib</artifactId> <version>2.4</version> <!--指定jdk版本 -->
	 * <classifier>jdk15</classifier> </dependency>
	 * 
	 * @param version
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testForJSONObject", method = RequestMethod.POST)
	public ModelMap testForJSONObject(@RequestBody JSONObject jsonObject) {
		String name = jsonObject.getString("name");
		logger.info("demoName：" + name);
		ModelMap map = new ModelMap();
		map.addAttribute("demoName", name);
		return map;
	}

	/**
	 * 输入 和输出为JSON格式的数据的方式 HttpEntity<?> ResponseEntity<?>
	 * 
	 * @param user
	 * @param request
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testForHttpEntity", method = RequestMethod.POST)
	public ResponseEntity<String> testForHttpEntity(HttpEntity<User> user, HttpServletRequest request,
			HttpSession session) {
		// 获取Headers方法
		HttpHeaders headers = user.getHeaders();

		// 获取内容
		String demoContent = user.getBody().getName();

		// 这里直接new一个对象（HttpHeaders headers = new HttpHeaders();）
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("MyHeaderName", "demo");

		ResponseEntity<String> responseResult = new ResponseEntity<String>(demoContent, responseHeaders, HttpStatus.OK);
		return responseResult;
	}

}
