package com.lyon.springboot.entity.factory;


import com.lyon.springboot.entity.base.FormMap;
import org.apache.poi.ss.formula.functions.T;

import java.util.HashMap;

public class FormmapFactory {  
    private static HashMap<T,  Object> hashMap=new  HashMap<T,  Object>();
    @SuppressWarnings({ "unchecked", "hiding" })  
    public static <T> T getBeanFormMap(Class<T> clazz) throws InstantiationException, IllegalAccessException{  
        if (hashMap.containsKey(clazz)) {  
            FormMap<String, Object> formMap = (FormMap<String, Object>) hashMap.get(clazz);
            T t=(T) hashMap.get(clazz);  
            formMap.clear();  
            return t;  
        }else {  
            T t=clazz.newInstance();  
            return t;  
        }  
    }  
      
}  