package com.lyon.springboot.entity.view;


import com.lyon.springboot.annotation.TableSeg;
import com.lyon.springboot.entity.base.FormMap;

/**
 * 企业客户详情
 * @author 李品良
 *
 */
@TableSeg(tableName ="datasource_func_space_entity_view", id = "id")
public class DatasourceFuncAllViewFormMap extends FormMap<String, Object> {

	private static final long serialVersionUID = 1L;
	
}
	