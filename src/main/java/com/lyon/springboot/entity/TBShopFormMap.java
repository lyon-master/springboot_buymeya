package com.lyon.springboot.entity;


import com.lyon.springboot.annotation.TableSeg;
import com.lyon.springboot.entity.base.FormMap;

/**
 * 实体表
 */
@TableSeg(tableName = "tblm_shop", id="id")
public class TBShopFormMap extends FormMap<String,Object> {

	/**
	 *@descript
	 *@author framework
	 *@date 2015年3月29日
	 *@version 1.0
	 */
	private static final long serialVersionUID = 1L;
	
}
