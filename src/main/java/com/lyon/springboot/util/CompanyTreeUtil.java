package com.lyon.springboot.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CompanyTreeUtil {
	
	
	/**
	 * 根据父节点的ID获取所有子节点
	 * @param list 分类表
	 * @param typeId 传入的父节点ID
	 * @return String
	 */
	public List<TreeObjectToCompany> getChildTreeObjects(List<TreeObjectToCompany> list,int praentId) {
		List<TreeObjectToCompany> returnList = new ArrayList<TreeObjectToCompany>();
		for (Iterator<TreeObjectToCompany> iterator = list.iterator(); iterator.hasNext();) {
			TreeObjectToCompany t = (TreeObjectToCompany) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId()==praentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}
	
	/**
	 * 递归列表
	 * @author framework
	 * Email: g5874589652@126.com
	 * @date 2013-12-4 下午7:27:30
	 * @param list
	 * @param TreeObjectToCompany
	 */
	private void  recursionFn(List<TreeObjectToCompany> list, TreeObjectToCompany t) {
		List<TreeObjectToCompany> childList = getChildList(list, t);// 得到子节点列表
		t.setChildren(childList);
		for (TreeObjectToCompany tChild : childList) {
			if (hasChild(list, tChild)) {// 判断是否有子节点
				//returnList.add(TreeObjectToCompany);
				Iterator<TreeObjectToCompany> it = childList.iterator();
				while (it.hasNext()) {
					TreeObjectToCompany n = (TreeObjectToCompany) it.next();
					recursionFn(list, n);
				}
			}
		}
	}
	
	// 得到子节点列表
	private List<TreeObjectToCompany> getChildList(List<TreeObjectToCompany> list, TreeObjectToCompany t) {
		
		List<TreeObjectToCompany> tlist = new ArrayList<TreeObjectToCompany>();
		Iterator<TreeObjectToCompany> it = list.iterator();
		while (it.hasNext()) {
			TreeObjectToCompany n = (TreeObjectToCompany) it.next();
			if (n.getParentId() == t.getId()) {
				tlist.add(n);
			}
		}
		return tlist;
	} 
	List<TreeObjectToCompany> returnList = new ArrayList<TreeObjectToCompany>();
	/**
     * 根据父节点的ID获取所有子节点
     * @param list 分类表
     * @param typeId 传入的父节点ID
     * @param prefix 子节点前缀
     */
    public List<TreeObjectToCompany> getChildTreeObjectToCompanys(List<TreeObjectToCompany> list, int typeId,String prefix){
        if(list == null) return null;
        for (Iterator<TreeObjectToCompany> iterator = list.iterator(); iterator.hasNext();) {
            TreeObjectToCompany node = (TreeObjectToCompany) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (node.getParentId()==typeId) {
                recursionFn(list, node,prefix);
            }
            // 二、遍历所有的父节点下的所有子节点
            /*if (node.getParentId()==0) {
                recursionFn(list, node);
            }*/
        }
        return returnList;
    }
     
    private void recursionFn(List<TreeObjectToCompany> list, TreeObjectToCompany node,String p) {
        List<TreeObjectToCompany> childList = getChildList(list, node);// 得到子节点列表
        if (hasChild(list, node)) {// 判断是否有子节点
            returnList.add(node);
            Iterator<TreeObjectToCompany> it = childList.iterator();
            while (it.hasNext()) {
                TreeObjectToCompany n = (TreeObjectToCompany) it.next();
                n.setCompanyName(p+n.getCompanyName());
                recursionFn(list, n,p+p);
            }
        } else {
            returnList.add(node);
        }
    }

	// 判断是否有子节点
	private boolean hasChild(List<TreeObjectToCompany> list, TreeObjectToCompany t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
	
	// 本地模拟数据测试
	public void main(String[] args) {
//		long start = System.currentTimeMillis();
//		List<TreeObjectToCompany> TreeObjectToCompanyList = new ArrayList<TreeObjectToCompany>();
//		CompanyTreeUtil mt = new CompanyTreeUtil();
//		List<TreeObjectToCompany> ns=mt.getChildTreeObjects(TreeObjectToCompanyList,0);
//		for (TreeObjectToCompany m : ns) {
//			System.out.println(m.getCompanyName());
//			System.out.println(m.getChildren());
//		}
//		long end = System.currentTimeMillis();
//		System.out.println("用时:" + (end - start) + "ms");
	}
	
}
