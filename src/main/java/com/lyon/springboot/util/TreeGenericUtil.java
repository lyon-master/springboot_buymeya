package com.lyon.springboot.util;


import com.lyon.springboot.entity.base.FormMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 把一个list集合,里面的bean含有 parentId 转为树形式
 *
 */
public class TreeGenericUtil {
	
	
	/**
	 * 根据父节点的ID获取所有子节点
	 * @param list 分类表
	 * @param typeId 传入的父节点ID
	 * @return String
	 */
	public List<FormMap<String,Object>> getChildTreeObjects(List<FormMap<String,Object>> list, int praentId) {
		List<FormMap<String,Object>> returnList = new ArrayList<FormMap<String,Object>>();
		for (Iterator<FormMap<String,Object>> iterator = list.iterator(); iterator.hasNext();) {
			FormMap<String,Object> t = (FormMap<String,Object>) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getInt("parentId")==praentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}
	/**
	 * 根据父节点的ID获取所有子节点
	 * @param list 分类表
	 * @param typeId 传入的父节点ID
	 * @return String
	 */
	public List<FormMap<String,Object>> getChildTreeObjectsParent(List<FormMap<String,Object>> list,int praentId,String parentIdTableName) {
		List<FormMap<String,Object>> returnList = new ArrayList<FormMap<String,Object>>();
		for (Iterator<FormMap<String,Object>> iterator = list.iterator(); iterator.hasNext();) {
			FormMap<String,Object> t = (FormMap<String,Object>) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getInt(parentIdTableName)==praentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}
	/**
	 * 递归列表
	 * @author framework
	 * Email: g5874589652@126.com
	 * @date 2013-12-4 下午7:27:30
	 * @param list
	 * @param FormMap<String,Object>
	 */
	private void  recursionFn(List<FormMap<String,Object>> list, FormMap<String,Object> t) {
		List<FormMap<String,Object>> childList = getChildList(list, t);// 得到子节点列表
		t.put("childList", childList);
		for (FormMap<String,Object> tChild : childList) {
			if (hasChild(list, tChild)) {// 判断是否有子节点
				//returnList.add(FormMap<String,Object>);
				Iterator<FormMap<String,Object>> it = childList.iterator();
				while (it.hasNext()) {
					FormMap<String,Object> n = (FormMap<String,Object>) it.next();
					recursionFn(list, n);
				}
			}
		}
	}
	
	// 得到子节点列表
	private List<FormMap<String,Object>> getChildList(List<FormMap<String,Object>> list, FormMap<String,Object> t) {
		
		List<FormMap<String,Object>> tlist = new ArrayList<FormMap<String,Object>>();
		Iterator<FormMap<String,Object>> it = list.iterator();
		while (it.hasNext()) {
			FormMap<String,Object> n = (FormMap<String,Object>) it.next();
			int parent=n.getInt("parentId");
			int id=t.getInt("id");
			if (parent==id) {
				
				tlist.add(n);
			}
		}
		return tlist;
	} 
	List<FormMap<String,Object>> returnList = new ArrayList<FormMap<String,Object>>();
	/**
     * 根据父节点的ID获取所有子节点
     * @param list 分类表
     * @param typeId 传入的父节点ID
     * @param prefix 子节点前缀
     */
    public List<FormMap<String,Object>> getChildTreeObjects(List<FormMap<String,Object>> list, int typeId,String prefix){
        if(list == null) return null;
        for (Iterator<FormMap<String,Object>> iterator = list.iterator(); iterator.hasNext();) {
            FormMap<String,Object> node = (FormMap<String,Object>) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (node.getInt("parentId")==typeId) {
                recursionFn(list, node,prefix);
            }
            // 二、遍历所有的父节点下的所有子节点
            /*if (node.getParentId()==0) {
                recursionFn(list, node);
            }*/
        }
        return returnList;
    }
     
    private void recursionFn(List<FormMap<String,Object>> list, FormMap<String,Object> node,String p) {
        List<FormMap<String,Object>> childList = getChildList(list, node);// 得到子节点列表
        if (hasChild(list, node)) {// 判断是否有子节点
            returnList.add(node);
            Iterator<FormMap<String,Object>> it = childList.iterator();
            while (it.hasNext()) {
                FormMap<String,Object> n = (FormMap<String,Object>) it.next();
                recursionFn(list, n,p+p);
            }
        } else {
            returnList.add(node);
        }
    }

	// 判断是否有子节点
	private boolean hasChild(List<FormMap<String,Object>> list, FormMap<String,Object> t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
	
	// 本地模拟数据测试
	public void main(String[] args) {
		/*long start = System.currentTimeMillis();
		List<FormMap<String,Object>> TreeObjectList = new ArrayList<FormMap<String,Object>>();
		
		TreeObjectUtil mt = new TreeObjectUtil();
		List<FormMap<String,Object>> ns=mt.getChildTreeObjects(TreeObjectList,0);
		for (FormMap<String,Object> m : ns) {
			System.out.println(m.getName());
			System.out.println(m.getChildren());
		}
		long end = System.currentTimeMillis();
		System.out.println("用时:" + (end - start) + "ms");*/
	}
	
}
