package com.lyon.springboot.util;/*
package com.framework.util;

import com.framework.entity.ConsoleuserFormMap;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class PasswordHelper {
	private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
	private String algorithmName = "md5";
	private int hashIterations = 2;

	public void encryptPassword(ConsoleuserFormMap userFormMap) {
		String salt=getSalt();
		userFormMap.put("credentialsSalt", salt);
		String newPassword = new SimpleHash(algorithmName, userFormMap.get("password"), ByteSource.Util.bytes(userFormMap.get("accountName")+salt), hashIterations).toHex();
		userFormMap.put("password", newPassword);
		System.out.println(newPassword);
	}



	public String encryptPassword(String accountName,String password) {
		String salt=getSalt();
		System.out.println(salt);
		String newPassword = new SimpleHash(algorithmName, password, ByteSource.Util.bytes(accountName+salt), hashIterations).toHex();
		return newPassword;
	}
	
	public String encryptPassword(String accountName,String password,String salt) {
		String newPassword = new SimpleHash(algorithmName, password, ByteSource.Util.bytes(accountName+salt), hashIterations).toHex();
		return newPassword;
	}
	
	public String getSalt() {
		String salt=randomNumberGenerator.nextBytes().toHex();
		return salt;
	}

	
	
	public static void main(String[] args) {
		UsernamePasswordToken token = new UsernamePasswordToken("12313421211", "123456");
		
//		String username="sysadmin";
//		String salt="ccabd478690551b55e66b14217f82362";
//		ByteSource byteSource=ByteSource.Util.bytes(username+salt);
//		System.out.println(byteSource);
		String newPassword = new SimpleHash("md5", "123456", ByteSource.Util.bytes("zzz"+"21b8615b883b956b8f5dbf45a9137ec4"), 2).toHex();
		System.out.println("newPassword="+newPassword);
		PasswordHelper passwordHelper = new PasswordHelper();
		ConsoleuserFormMap userFormMap = new ConsoleuserFormMap();
		userFormMap.put("password","5601564aaa...");
		userFormMap.put("accountName","admin");
		passwordHelper.encryptPassword(userFormMap);
		System.out.println(userFormMap);
		System.out.println(ByteSource.Util.bytes("admin4157c3feef4a6ed91b2c28cf4392f2d1"));
	}
}
*/
