package com.lyon.springboot.util;

import java.util.ArrayList;
import java.util.List;

public class TreeObjectToCompany {
	private Integer id;
	private Integer parentId;
	private String companyName;
	private Integer companyCode;
	private Integer companyType;
	private Integer companyRank;
	private String companyAdderss;
	private String companyPostcode;
	private String companyPrincipal;
	private String companyTel;
	private String companyFox;
	private String companyEmail;
	private String reMark;
	private List<TreeObjectToCompany> children = new ArrayList<TreeObjectToCompany>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(Integer companyCode) {
		this.companyCode = companyCode;
	}
	public Integer getCompanyType() {
		return companyType;
	}
	public void setCompanyType(Integer companyType) {
		this.companyType = companyType;
	}
	public Integer getCompanyRank() {
		return companyRank;
	}
	public void setCompanyRank(Integer companyRank) {
		this.companyRank = companyRank;
	}
	public String getCompanyAdderss() {
		return companyAdderss;
	}
	public void setCompanyAdderss(String companyAdderss) {
		this.companyAdderss = companyAdderss;
	}
	public String getCompanyPostcode() {
		return companyPostcode;
	}
	public void setCompanyPostcode(String companyPostcode) {
		this.companyPostcode = companyPostcode;
	}
	public String getCompanyPrincipal() {
		return companyPrincipal;
	}
	public void setCompanyPrincipal(String companyPrincipal) {
		this.companyPrincipal = companyPrincipal;
	}
	public String getCompanyTel() {
		return companyTel;
	}
	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}
	public String getCompanyFox() {
		return companyFox;
	}
	public void setCompanyFox(String companyFox) {
		this.companyFox = companyFox;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getReMark() {
		return reMark;
	}
	public void setReMark(String reMark) {
		this.reMark = reMark;
	}
	public List<TreeObjectToCompany> getChildren() {
		return children;
	}
	public void setChildren(List<TreeObjectToCompany> children) {
		this.children = children;
	}
}
